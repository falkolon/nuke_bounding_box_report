"""Menu integration."""

from nukescripts import panels


def get_bounding_box_report_widget():
    import bounding_box_report

    return bounding_box_report.get_gui()


panels.registerWidgetAsPanel(
    "get_bounding_box_report_widget",
    "Bounding Box Report",
    "de.filmkorn.bounding_box_report",
    False,
)
