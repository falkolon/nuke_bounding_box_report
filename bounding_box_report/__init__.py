from bounding_box_report.gui import controller

_CONTROLLER = None


def show_gui():
    """Show the GUI."""
    global _CONTROLLER

    _CONTROLLER = controller.Controller()
    _CONTROLLER.show_gui()

    return _CONTROLLER


def get_gui():
    global _CONTROLLER

    _CONTROLLER = controller.Controller()
    return _CONTROLLER.get_gui()
